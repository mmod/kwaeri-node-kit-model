/**
 * SPDX-PackageName: kwaeri/model
 * SPDX-PackageVersion: 0.3.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import { Driver } from '@kwaeri/driver';
import { BaseDatabaseDriver, DatabaseDriver } from '@kwaeri/database-driver';
import debug from 'debug';


// DEFINES
const DEBUG = debug( 'nodekit:model' );


export class Model {
    /**
     * @var configuration
     */
    configuration;


    /**
     * @var connector
     */
    connector;


    /**
     * @var driver
     */
    driver: any;


    /**
     *
     *
     *
     * .
     * Class constructor
     */
    constructor( configuration: any ) {
        this.driver = configuration.driver;

        this.connector = ( configuration.connector ) ? configuration.connector : DatabaseDriver;

        this.configuration = configuration.database;
    }


    /**
     * Gets an instance of the DBO
     */
    dbo  = () => {
        const configuration = {
            type:       this.configuration.type,
            host:       this.configuration.host,
            port:       +this.configuration.port,
            database:   this.configuration.database,
            user:       this.configuration.user,
            password:   this.configuration.password
        };
        //configuration.model = this.schema;

        DEBUG( `New 'class Driver'` );

        // Instantiate a driver each time??
        const driver = new this.driver( configuration, this.connector );

        // Return it -
        return driver.get();
    };


    /**
     * Extends the base model with derived properties
     */
    set( child: any ) {
        // Prepare to 'set' up our model:
        const model = child;

        DEBUG( `Set 'derived' to '${model}'`)

        // It's all about composition...right?  :)
        model.dbo = this.dbo.bind( this );

        // Send it back::
        return model;
    };
}
